package com.chinmay.jforcesolution.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.chinmay.jforcesolution.model.UserLogin;


public interface UserLoginRepo extends JpaRepository<UserLogin, Integer> {
//	select * from user_registration ur where ur.user_name = ?1
//	select * from user_login ul where ul.session_id = ?1 
	@Query(value = "select * from user_login ul where ul.session_id =  ?1 " ,nativeQuery = true)
	public UserLogin isValidSessionId(String sessionId);
	
	@Query(value = "select * from user_login ul where ul.user_id =  ?1 " ,nativeQuery = true)
	public List<UserLogin> userReportByUserId(String userId);
}
 