package com.chinmay.jforcesolution.repo;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.chinmay.jforcesolution.model.UserRegistration;

public interface UserRegistrationRepo extends JpaRepository<UserRegistration, UUID> {

    @Query(value = "select * from user_registration ur where ur.user_name = ?1 or ur.email = ?1 or ur.mobile_number = ?1 and ur.password = ?2 ",nativeQuery = true)
	public UserRegistration isRegisteredUser(String userName , String password);
    
//    @Query(value = "SELECT \r\n"
//    		+ "    CASE WHEN user_name = 'user1' THEN 'user_name' ELSE NULL END AS user_name_flag,\r\n"
//    		+ "    CASE WHEN email = 'user1@gmail.com' THEN 'email' ELSE NULL END AS email_flag,\r\n"
//    		+ "    CASE WHEN mobile_number = '5987956204' THEN 'mobile_number' ELSE NULL END AS mobile_number_flag\r\n"
//    		+ "FROM \r\n"
//    		+ "    user_registration\r\n"
//    		+ "WHERE \r\n"
//    		+ "    user_name = 'user2' \r\n"
//    		+ "    OR email = 'user2@gmail.com' \r\n"
//    		+ "    OR mobile_number = '5987956204';", nativeQuery = true)
//    public String isRegisteredUser(String userName,String email,String phone);
    
    @Query(value = "SELECT \r\n"
            + "    CASE WHEN user_name = :userName THEN 'UserName' ELSE NULL END AS user_name_flag,\r\n"
            + "    CASE WHEN email = :email THEN 'EMAIL' ELSE NULL END AS email_flag,\r\n"
            + "    CASE WHEN mobile_number = :phone THEN 'PhoneNumber' ELSE NULL END AS mobile_number_flag\r\n"
            + "FROM \r\n"
            + "    user_registration\r\n"
            + "WHERE \r\n"
            + "    user_name = :userName \r\n"
            + "    OR email = :email \r\n"
            + "    OR mobile_number = :phone", nativeQuery = true)
    public String isRegisteredUser(@Param("userName") String userName, @Param("email") String email, @Param("phone") String phone);


}
