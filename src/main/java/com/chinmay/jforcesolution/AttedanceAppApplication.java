package com.chinmay.jforcesolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AttedanceAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(AttedanceAppApplication.class, args);
	}

}
