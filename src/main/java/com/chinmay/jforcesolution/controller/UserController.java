package com.chinmay.jforcesolution.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chinmay.jforcesolution.model.UserLogin;
import com.chinmay.jforcesolution.model.UserRegistration;
import com.chinmay.jforcesolution.service.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*" , originPatterns = "*")
public class UserController {
	
	@Autowired
	UserService registrationService;
	
	@PostMapping("/register-user")
	public ResponseEntity<?> registerUser(@RequestBody UserRegistration userRegistration) {
		return registrationService.registerUser(userRegistration);
	}
	
	@PostMapping("/log-in")
	public ResponseEntity<?> logInUser(@RequestBody UserRegistration userLogin) {
		return registrationService.logInUser(userLogin);
	}
	
	@PostMapping("/log-out")
	public ResponseEntity<?> logOutUser(@RequestBody UserLogin sessionId) {
		return registrationService.logOutUser(sessionId);
	}
	
	@PostMapping("/view-report")
	public ResponseEntity<?> userReportByUserId(@RequestBody UserLogin userId) {
		return registrationService.userReportByUserId(userId);
	}
	
	@GetMapping("/all-users")
	public ResponseEntity<?> fetchAllUsers() {
		return registrationService.fetchAllUsers();
	}
	
	@PostMapping("/user-details")
	public ResponseEntity<?> getUserDetailBySessionId(@RequestBody UserLogin userLogin){
		return registrationService.getUserDetailBySessionId(userLogin);
	}
}
