package com.chinmay.jforcesolution.model;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "user_login")
public class UserLogin {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String userName;
	
	private String password;
	
	private ZonedDateTime log_in_at;
	
	private ZonedDateTime log_out_at;
	
	private LocalDate  logInDate;
	
	private LocalDate  logOutDate;
	
	
	private String userId;
	
	private String sessionId;
	
	
	
	
	
	public LocalDate getLogInDate() {
		return logInDate;
	}

	public void setLogInDate(LocalDate logInDate) {
		this.logInDate = logInDate;
	}

	public LocalDate getLogOutDate() {
		return logOutDate;
	}

	public void setLogOutDate(LocalDate logOutDate) {
		this.logOutDate = logOutDate;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ZonedDateTime getLog_in_at() {
		return log_in_at;
	}

	public void setLog_in_at(ZonedDateTime log_in_at) {
		this.log_in_at = log_in_at;
	}

	public ZonedDateTime getLog_out_at() {
		return log_out_at;
	}

	public void setLog_out_at(ZonedDateTime log_out_at) {
		this.log_out_at = log_out_at;
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
