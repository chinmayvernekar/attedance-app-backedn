package com.chinmay.jforcesolution.model;

import java.util.Date;
import java.util.UUID;

public class UserDetails {

	private Date log_in_at;

	private Date log_out_at;

	private UUID userId;

	private String sessionId;

	public Date getLog_in_at() {
		return log_in_at;
	}

	public void setLog_in_at(Date log_in_at) {
		this.log_in_at = log_in_at;
	}

	public Date getLog_out_at() {
		return log_out_at;
	}

	public void setLog_out_at(Date log_out_at) {
		this.log_out_at = log_out_at;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
}
