package com.chinmay.jforcesolution.model;

import java.time.ZonedDateTime;

public class LogInResponse {
	
	private String message;
	
	private String sessionId;
	
	private String userId;
	
	private ZonedDateTime log_in_at;
	
	private ZonedDateTime log_out_at;
	
	

	public ZonedDateTime getLog_in_at() {
		return log_in_at;
	}

	public void setLog_in_at(ZonedDateTime log_in_at) {
		this.log_in_at = log_in_at;
	}

	public ZonedDateTime getLog_out_at() {
		return log_out_at;
	}

	public void setLog_out_at(ZonedDateTime log_out_at) {
		this.log_out_at = log_out_at;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
}
