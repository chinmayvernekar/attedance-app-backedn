package com.chinmay.jforcesolution.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.chinmay.jforcesolution.model.LogInResponse;
import com.chinmay.jforcesolution.model.UserLogin;
import com.chinmay.jforcesolution.model.UserRegistration;
import com.chinmay.jforcesolution.model.UserRegistrationResponse;
import com.chinmay.jforcesolution.repo.UserLoginRepo;
import com.chinmay.jforcesolution.repo.UserRegistrationRepo;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRegistrationRepo registrationRepo;

	@Autowired
	UserLoginRepo userLoginRepo;

	@Override
	public ResponseEntity<?> registerUser(UserRegistration userRegistration) {
		UserRegistrationResponse resp = new UserRegistrationResponse();
		UUID userId = UUID.randomUUID();
		String isRegisteredUser = registrationRepo.isRegisteredUser(userRegistration.getUserName(),
				userRegistration.getEmail(), userRegistration.getMobileNumber());
		try {
			UserRegistration registration = new UserRegistration();

			if (isRegisteredUser == null) {
				registration.setEmail(userRegistration.getEmail());
				registration.setPassword(userRegistration.getPassword());
				registration.setUserName(userRegistration.getUserName());
				registration.setMobileNumber(userRegistration.getMobileNumber());
				registration.setUserId(userId);
				resp.setMessage("User Registration Sucessfull.");
				registrationRepo.save(registration);
				return ResponseEntity.status(HttpStatus.CREATED).body(resp);
			} else {

				if (isRegisteredUser.contains("UserName,EMAIL,PhoneNumber")) {
					resp.setMessage(
							"User is already registered with us with same " + isRegisteredUser + ". Please LogIn.");
				} else {
					String result = isRegisteredUser.replace("null", "").replace(",", " ");
					resp.setMessage(
							"User is already registered with us with same " + ".Please use unique " + result);
				}

				return ResponseEntity.status(HttpStatus.OK).body(resp);
			}
		} catch (Exception e) {
			resp.setMessage("Something Went Wrong.Please try after sometime." + e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
	}

	@Override
	public ResponseEntity<?> logInUser(UserRegistration userLogin) {
		LogInResponse resp = new LogInResponse();
		try {
			UserRegistration isRegisteredUser = registrationRepo.isRegisteredUser(userLogin.getUserName(),
					userLogin.getPassword());
			ZoneId serverZoneId = ZoneId.systemDefault();
			ZonedDateTime serverTime = ZonedDateTime.now(serverZoneId);
			if (isRegisteredUser != null) {
				UserLogin userLogin_Rec = new UserLogin();
				userLogin_Rec.setSessionId(UUID.randomUUID().toString());
				userLogin_Rec.setUserId(isRegisteredUser.getUserId().toString());
				userLogin_Rec.setLog_in_at(serverTime);
				userLogin_Rec.setLogInDate(LocalDate.now());
				resp.setMessage("User Session Started");
				resp.setSessionId(userLogin_Rec.getSessionId().toString());
				resp.setUserId(userLogin_Rec.getUserId().toString());
				resp.setLog_in_at(userLogin_Rec.getLog_in_at());
				userLoginRepo.save(userLogin_Rec);
			} else {
				return ResponseEntity.ok("UserName is not registered with us.");
			}

		} catch (Exception e) {
			System.out.println("Exception " + e);
		}

		return ResponseEntity.ok(resp);
	}

	@Override
	public ResponseEntity<?> logOutUser(UserLogin sessionId) {
		try {
			ZoneId serverZoneId = ZoneId.systemDefault();
			ZonedDateTime serverTime = ZonedDateTime.now(serverZoneId);
			UserLogin userLogOut = userLoginRepo.isValidSessionId(sessionId.getSessionId());
			userLogOut.setLogOutDate(LocalDate.now());
			userLogOut.setSessionId(null);
			userLogOut.setLog_out_at(serverTime);
			userLoginRepo.save(userLogOut);
		} catch (Exception e) {
			System.out.println(e);
			return ResponseEntity.ok("Something Went Wrong.");
		}
		return ResponseEntity.ok("User Session Ended");
	}

	@Override
	public ResponseEntity<?> userReportByUserId(UserLogin userId) {
		try {
			List<UserLogin> userLogOut = userLoginRepo.userReportByUserId(userId.getUserId());
			return ResponseEntity.ok(userLogOut);
		} catch (Exception e) {

		}
		return null;
	}

	public Boolean checkIfAdmin(String userName, String password) {

		if (userName.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}
	
	public ResponseEntity<?> getUserDetailBySessionId(UserLogin userLogin) {
		
		try {
			UserLogin login = userLoginRepo.isValidSessionId(userLogin.getSessionId());
			return ResponseEntity.status(HttpStatus.OK).body(login);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
	}
	
	@Override
	public ResponseEntity<?> fetchAllUsers() {
		try {
			return ResponseEntity.ok(registrationRepo.findAll());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
}
