package com.chinmay.jforcesolution.service;

import org.springframework.http.ResponseEntity;

import com.chinmay.jforcesolution.model.UserLogin;
import com.chinmay.jforcesolution.model.UserRegistration;

public interface UserService {
	
	public ResponseEntity<?> registerUser(UserRegistration userRegistration);
	
	public ResponseEntity<?> logInUser(UserRegistration userLogin);
	
	public ResponseEntity<?> logOutUser(UserLogin sessionId);
	
	public ResponseEntity<?> userReportByUserId(UserLogin userId);
	
	public ResponseEntity<?> fetchAllUsers();
	
	public ResponseEntity<?> getUserDetailBySessionId(UserLogin userLogin);
}
